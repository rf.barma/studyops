﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace StudyOps.Models
{
    public class TeamContext : DbContext
    {
        public DbSet<Team> Team { get; set; }
        public TeamContext(DbContextOptions<TeamContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
