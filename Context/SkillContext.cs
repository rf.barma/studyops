﻿using Microsoft.EntityFrameworkCore;
using StudyOps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudyOps.Context
{
    public class SkillContext : DbContext
    {
        public DbSet<Skill> Skill { get; set; }
        public SkillContext(DbContextOptions<SkillContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
