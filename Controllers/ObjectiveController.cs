﻿using Microsoft.AspNetCore.Mvc;
using StudyOps.Context;
using StudyOps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudyOps.Controllers
{
    [Route("api/objective")]
    [ApiController]
    public class ObjectiveController : ControllerBase
    {
        ObjectiveContext db;
        public ObjectiveController(ObjectiveContext context)
        {
            db = context;

        }

        // DELETE api/task/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Objective>> Delete(int id)
        {
            Objective task = db.Objective.FirstOrDefault(x => x.Id == id);
            if (task == null)
            {
                return NotFound();
            }
            db.Objective.Remove(task);
            await db.SaveChangesAsync();
            return Ok(task);
        }
    }
}
