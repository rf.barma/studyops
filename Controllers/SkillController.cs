﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StudyOps.Context;
using StudyOps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudyOps.Controllers
{
    
    [Route("api/skill")]
    [ApiController]
    public class SkillController : ControllerBase
    {
        SkillContext db;
        public SkillController(SkillContext context)
        {
            db = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Skill>>> Get()
        {
            return await db.Skill.ToListAsync();
        }

        // GET api/skill/5
        [HttpGet("{id}")]
        public ActionResult TeamDetails(int? id)
        {

            var jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore,
                MaxDepth = null,
            };

            if (id == null)
            {
                return BadRequest();
            }
            Skill skill = db.Skill.Include(t => t.Tasks).FirstOrDefault(t => t.Id == id);
            if (skill == null)
            {
                return BadRequest();
            }
            return Ok(skill);
        }
        // PUT api/skill/
        [HttpPut]
        public async Task<ActionResult<Skill>> Put(Skill skill)
        {
            if (skill == null)
            {
                return BadRequest();
            }
            if (!db.Skill.Any(x => x.Id == skill.Id))
            {
                return NotFound();
            }

            db.Update(skill);
            await db.SaveChangesAsync();
            return Ok(skill);
        }
    }
}
