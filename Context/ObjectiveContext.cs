﻿using Microsoft.EntityFrameworkCore;
using StudyOps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudyOps.Context
{
    public class ObjectiveContext : DbContext
    {
        public DbSet<Objective> Objective { get; set; }
        public ObjectiveContext(DbContextOptions<ObjectiveContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
