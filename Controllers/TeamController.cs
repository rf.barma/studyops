﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudyOps.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Authorization;

namespace StudyOps.Controllers
{
    
    [Route("api/team")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        TeamContext db;
        public TeamController(TeamContext context)
        {
            db = context;
            
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Team>>> Get()
        {
            return await db.Team.ToListAsync();
        }

        // GET api/team/5
        [HttpGet("{id}")]
        public ActionResult TeamDetails(int? id)
        {

            var jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore,
                MaxDepth = null,
            };

            if (id == null)
            {
                return BadRequest();
            }
            Team team = db.Team.Include(t => t.Users).FirstOrDefault(t => t.Id == id);
            if (team == null)
            {
                return BadRequest();
            }
            return Ok(team);
        }

        

        // POST api/team
        [HttpPost]
        public async Task<ActionResult<Team>> Post(Team team)
        {
            if (team == null)
            {
                return BadRequest();
            }

            db.Team.Add(team);
            await db.SaveChangesAsync();
            return Ok(team);
        }

        // PUT api/team/
        [HttpPut]
        public async Task<ActionResult<Team>> Put(Team team)
        {
            if (team == null)
            {
                return BadRequest();
            }
            if (!db.Team.Any(x => x.Id == team.Id))
            {
                return NotFound();
            }

            db.Update(team);
            await db.SaveChangesAsync();
            return Ok(team);
        }

        // DELETE api/team/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> Delete(int id)
        {
            Team team = db.Team.FirstOrDefault(x => x.Id == id);
            if (team == null)
            {
                return NotFound();
            }
            db.Team.Remove(team);
            await db.SaveChangesAsync();
            return Ok(team);
        }
    }
}
